<?php
/*
Template Name: Présentation
Template Post Type: presentation
*/
?>
<section>
    <div class="container">
      <div class="row">

<?php
$pstloop = new WP_Query(
    array(
        'post_type' => 'presentation',
        'posts_per_page' => 3
    )
);
while ( $pstloop->have_posts() ) : $pstloop->the_post();
?>
    <div class="col-lg-4">
    <?php the_post_thumbnail()?>
          <h3><?php the_title();?></h3>
          <p><?php the_content();?></p>
        </div>

<?php endwhile;
wp_reset_postdata();
?>
    </div>
  </section>