<?php
/*
Template name: Image
Template Post Type: img
*/
?>
    <div class="container">
      <div class="row">
    
<?php
$imgloop = new WP_Query(
    array(
        'post_type' => 'img',
        'posts_per_page' => 1
    )
);
while ( $imgloop->have_posts() ) : $imgloop->the_post();
?>
        <div class="col-lg-12">
            <?php the_post_thumbnail()?>
        </div>

<?php endwhile;
wp_reset_postdata();
?>
        <div class="line"></div>
    </div>
  </section>