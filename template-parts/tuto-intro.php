<?php
/*
Template name: tuto-intro
Template Post Type: intro-tuto
*/
?>
    <div class="container">
      <div class="row">
    
<?php
$tintroloop = new WP_Query(
    array(
        'post_type' => 'intro-tuto',
        'posts_per_page' => 1
    )
);
while ( $tintroloop->have_posts() ) : $tintroloop->the_post();
?>
        <div class="col-lg-12">
            <?php the_content();?>
        </div>

<?php endwhile;
wp_reset_postdata();
?>
        <div class="line"></div>
    </div>
  </section>