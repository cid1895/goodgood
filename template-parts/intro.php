<?php
/*
Template Name: Introduction
Template Post Type: intro, tutoriel
*/
?>
<section>
    <div class="container">
      <div class="row">
<?php
$introloop = new WP_Query(
  array(
      'post_type' => 'intro',
      'posts_per_page' => 1
  )
);
while ( $introloop->have_posts() ) : $introloop->the_post();
?>
        <div class="col-lg-6">
        <?php the_post_thumbnail()?>
        </div>
        <div class="col-lg-5">
          <p><?php the_content();?></p>
        </div>
      </div>
<?php endwhile;
wp_reset_postdata();
?>
      <div class="line"></div>
    </div>
  </section>