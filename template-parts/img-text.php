<?php
/*
Template Name: Img Text
Template Post Type: tutoriel
*/
?>
<section>
    <div class="container">
      <div class="row">
<?php
$tutoloop = new WP_Query(
  array(
      'post_type' => 'tutoriel',
      'posts_per_page' => 1
  )
);
while ( $tutoloop->have_posts() ) : $tutoloop->the_post();
?>
        <div class="col-lg-6">
        <?php the_post_thumbnail()?>
        </div>
        <div class="col-lg-5">
          <p><?php the_content();?></p>
        </div>
      </div>
<?php endwhile;
wp_reset_postdata();
?>
    </div>
  </section>