const btn = document.querySelector(".btn");

btn.addEventListener("click", function () {
    document.body.classList.toggle("dark");
    if (this.textContent == "Dark Mode") {
        this.textContent = "light Mode";
        this.classList.toggle("dark-btn");
    } else {
        this.textContent = "Dark Mode";
        this.classList.toggle("dark-btn");
    }
});