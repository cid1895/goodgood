<?php
/*
  Template Name: Tutoriel
*/
get_header();
?>
 <div class="hero-banner">
      <h2 class="logo">Good<span>Good</span></h2>
      <h3><?php the_content();?></h3>
    </div>
<?php
get_template_part('template-parts/tuto-intro', get_post_type());
get_template_part('template-parts/img-center', get_post_type());
get_template_part('template-parts/img-text', get_post_type());
get_template_part('template-parts/presentation', get_post_type());
get_footer();